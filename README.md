# LSCSoft MacPorts Repository

This repository contains the MacPorts source definitions for both stable
and testing LSC Software.

The testing directory is for testing LSC specific software before it is
ready for production use and the stable directory contains production
ready LSC specific software that isn't suitable for the main MacPorts
repository.

Once commits are pushed into this repository they are automatically
synced to the rsync server running on http://software.ligo.org.

Testing is available from `rsync://software.ligo.org::macports-testing`
and stable from `rsync://software.ligo.org::macports`
