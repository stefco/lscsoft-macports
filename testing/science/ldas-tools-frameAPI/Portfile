# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4

PortSystem    1.0
PortGroup     compiler_blacklist_versions 1.0

name          ldas-tools-frameAPI
version       2.6.3
categories    science
license       GPL-2+
platforms     darwin
maintainers   {@emaros ligo.org:ed.maros} openmaintainer

license       GPL-2+
description   Filters library used by ldas-tools
long_description ${description}

homepage      https://wiki.ligo.org/DASWG/LDASTools
master_sites  http://software.ligo.org/lscsoft/source/

checksums     rmd160 443fb78aeb252dca3dad8a7ada2bf0e4aaf738c3 \
              sha256 910c3b94e78bdf097b30cdce9f9544b275d076bc1c0d1d69c37364a14da8e336

depends_build  port:pkgconfig \
               port:boost
depends_lib    port:ldas-tools-ldasgen \
               port:ldas-tools-filters \
               port:ldas-tools-framecpp

configure.args --disable-warnings-as-errors \
               --disable-silent-rules \
               --with-optimization=high \
               --disable-tcl \
               --disable-python \
               --without-doxygen \
               --without-dot \
               --disable-latex

if {${configure.cxx_stdlib} eq "libstdc++" } {
    configure.args-append --disable-cxx11
}

# requires clang from Xcode5 or higher to build
compiler.blacklist-append {clang < 500.2.75} llvm-gcc-4.2 gcc-4.2

use_parallel_build yes

pre-fetch {
    if {${os.platform} eq "darwin" && ${os.major} < 11} {
        ui_error "${name} only runs on Mac OS X 10.7 or greater."
        return -code error "incompatible Mac OS X version"
    }
}

#------------------------------------------------------------------------

livecheck.type   regex
livecheck.url    ${master_sites}
livecheck.regex  {ldas-tools-frameAPI-(\d+(?:\.\d+)*).tar.gz}
